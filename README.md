# justo-plugin-cli

Plugin for running commands from the shell.

*Proudly made with ♥ in Valencia, Spain, EU.*

See `README.js.md` for knowing how to use in **JavaScript**.
