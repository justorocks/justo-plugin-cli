# justo.plugin.cli

**Justo** plugin for running commands from the shell.
This is local and SSH-compatible.

*Proudly made with ♥ in Valencia, Spain, EU.*

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

# Use

```
const cli = require("justo.plugin.cli");
```

## cli task

This task runs a command from the shell:

```
cli({cmd, bg, status, env, wd})
```

- `cmd` (string, required). Command to run.
- `detach` (bool). Must the command be executed in background? Default: `false`.
- `status` (number, string or number[]). Exit code(s) to wait. Default: `0`.
  If `any`, status not considered.
- `env` (object). Environment variables.
- `wd` (string). Work directory.

Example:

```
cli({cmd: "luacheck .", status: 0})
cli({cmd: "luacheck .", status: "any"})
cli({cmd: "luarocks make --local", status: [0, 1]})
```
