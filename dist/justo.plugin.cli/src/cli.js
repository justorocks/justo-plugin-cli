"use strict";

var _dogmalang = require("dogmalang");

const child_process = _dogmalang.dogma.use(require("child_process"));
function cli(params) {
  _dogmalang.dogma.paramExpectedToHave("params", params, { cmd: { type: _dogmalang.text, mandatory: true }, detach: { type: _dogmalang.bool, mandatory: false }, wd: { type: _dogmalang.text, mandatory: false }, status: { type: [_dogmalang.list, _dogmalang.text, _dogmalang.num], mandatory: false }, env: { type: _dogmalang.map, mandatory: false } });if (params.status != null) params.status = (0, _dogmalang.list)(params.status);{
    let res;_dogmalang.dogma.update(params, { name: "status", visib: ".", assign: "?=", value: [0] }, { name: "print", visib: ".", assign: "?=", value: _dogmalang.print }, { name: "printe", visib: ".", assign: "?=", value: console.error });const opts = { ["encoding"]: "utf-8" };if (params.wd) {
      opts.cwd = params.wd;
    }if (params.env) {
      opts.env = params.env;
    }if (params.stdin) {
      opts.input = params.stdin;
    }if (params.detach) {
      res = (0, _dogmalang.exec)(params.cmd, { ["detach"]: true });
    } else {
      res = child_process.execSync(params.cmd, opts);
    }if (res.error) {
      _dogmalang.dogma.raise(res.error);
    }if (params.detach) {
      if (res.stdout) {
        res.stdout = res.stdout.toString();
      }if (res.stderr) {
        res.stderr = res.stderr.toString();
      }if (params.output) {
        if (res.stdout != "" && res.stdout != "\n") {
          params.print(res.stdout);
        }if (res.stderr != "" && res.stderr != "\n") {
          params.printe(res.stderr);
        }
      }if (params.status != "any" && !_dogmalang.dogma.includes(params.status, res.status)) {
        _dogmalang.dogma.raise("exit code expected to be %s; received: %s.", params.status, res.status);
      }
    }return { ["status"]: res.status, ["stdout"]: res.stdout, ["stderr"]: res.stderr };
  }
}module.exports = exports = cli;