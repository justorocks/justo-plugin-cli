"use strict";

var _dogmalang = require("dogmalang");

module.exports = exports = _dogmalang.dogma.use(require("justo")).simple({ ["id"]: "cli", ["desc"]: "Run a command from the shell.", ["fmt"]: params => {
    _dogmalang.dogma.paramExpected("params", params, null);{
      return (0, _dogmalang.fmt)("Run '%s'", params.cmd);
    }
  } }, _dogmalang.dogma.use(require("./cli")));